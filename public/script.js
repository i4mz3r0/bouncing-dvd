let can=document.getElementById("can");
let c=can.getContext("2d");

let img=new Image();
let fontSize=50;
let x=can.width/2-100+(Math.round(Math.random()*200));
let y=can.height/2-100+(Math.round(Math.random()*200));
let xdir=Math.round(Math.random())*2-1;
let ydir=Math.round(Math.random())*2-1;
let speed=1;
let interval;
let hits=corners=cheat=0;
c.fillStyle="#fff";

start=(event)=>{
    if(!event) img.src="dvd.png";
    else img.src=URL.createObjectURL(event.target.files[0]);
    imgWidth=100;
    imgHeight=Math.round(img.height/img.width*100);
    img.onload=()=>{
    bouncing=()=>{
        c.clearRect(0,0,can.width,can.height);
        c.font="20px Arial, Helvetica, sans-serif"
        c.fillText("Hits: "+hits,30,can.height-60);
        c.fillText("Corners: "+corners,30,can.height-30);
        c.drawImage(img,x,y,imgWidth,imgHeight);
        if(cheat) x=(can.width-imgWidth)*(xdir+1)/2-(can.height-imgHeight)*(ydir+1)*xdir/2+(xdir*ydir*y);
        if(x<=0||x>=can.width-imgWidth){
            xdir=-xdir;
            hits++;
        }
        if(y<=0||y>=can.height-imgHeight){
            ydir=-ydir;
            hits++;
        }
        if((x<=0||x>=can.width-imgWidth)&&(y<=0||y>=can.height-imgHeight)){
            console.log("CORNER!!!!");
            corners++;
            cheat=0;
        }
        x+=xdir*speed;y+=ydir*speed;
    }
    setTimeout(bouncing,10);
    if(!interval) interval=setInterval(bouncing,10);
    }
}